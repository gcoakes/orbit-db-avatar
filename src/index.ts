export default function avatar(pub: string, size = 800): Promise<Blob | null> {
  const bg = `#${pub.slice(2, 8)}aa`;

  type Bubble = {
    x: number;
    y: number;
    radius: number;
    color: string;
  };

  const bubbles: Bubble[] = [];
  for (let idx = 8; idx + 14 < pub.length; idx += 14) {
    bubbles.push({
      x: parseInt(pub.slice(idx, idx + 2), 16) / 256.0,
      y: parseInt(pub.slice(idx + 2, idx + 4), 16) / 256.0,
      radius: parseInt(pub.slice(idx + 4, idx + 6), 16) / 256.0,
      color: "#" + pub.slice(idx + 6, idx + 14),
    });
  }

  const canvas: HTMLCanvasElement = document.createElement("canvas");
  canvas.width = size;
  canvas.height = size;

  const ctx = canvas.getContext("2d");

  if (!ctx) {
    throw new Error("Unable to get 2d context");
  }

  // Draw the background main circle.
  ctx.beginPath();
  ctx.arc(size / 2.0, size / 2.0, size / 2.0, 0, 2 * Math.PI);
  ctx.fillStyle = bg;
  ctx.closePath();
  ctx.fill();

  // Draw the bubbles.
  for (const { x, y, radius, color } of bubbles) {
    console.log(color);
    ctx.beginPath();
    ctx.arc((x * size) / 2.0, y * size, (radius * size) / 4.0, 0, 2 * Math.PI);
    ctx.arc(
      size - (x * size) / 2.0,
      y * size,
      (radius * size) / 4.0,
      0,
      2 * Math.PI
    );
    ctx.fillStyle = color;
    ctx.closePath();
    ctx.fill();
  }

  return new Promise((resolve) => canvas.toBlob(resolve));
}
