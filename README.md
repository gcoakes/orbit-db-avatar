# orbit-db-avatar

Generate a unique avatar for a given OrbitDB public key.

## Example

``` typescript
import avatar from "orbit-db-avatar";

const avatar: Blob = await avatar(orbitdb.identity.publicKey);
document.querySelector("#avatar").src = URL.createObjectURL(avatar);
```
